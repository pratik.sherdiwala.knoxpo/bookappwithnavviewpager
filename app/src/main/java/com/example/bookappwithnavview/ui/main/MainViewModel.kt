package com.example.bookappwithnavview.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookappwithnavview.Event
import com.example.bookappwithnavview.R
import com.example.bookappwithnavview.data.repository.Bookrepository
import com.example.bookappwithnavview.ui.book.BookFragment
import javax.inject.Inject

class MainViewModel @Inject constructor() : ViewModel() {

    private val _showBooksEvent = MutableLiveData<Event<Unit>>()
    val showBooksEvent: LiveData<Event<Unit>>
        get() = _showBooksEvent

    fun openBookFragment() {
        _showBooksEvent.value = Event(Unit)
    }

    private val _showCategoryEvent = MutableLiveData<Event<Unit>>()
    val showCategoryEvent: LiveData<Event<Unit>>
        get() = _showCategoryEvent

    fun openCategoryFragment() {
        _showCategoryEvent.value = Event(Unit)
    }
}