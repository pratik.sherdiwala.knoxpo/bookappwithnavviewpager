package com.example.bookappwithnavview.ui.main

import android.os.Bundle
import android.view.MenuItem
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.bookappwithnavview.R
import com.example.bookappwithnavview.databinding.ActivityMainBinding
import com.example.bookappwithnavview.ui._common.DataBindingActivity
import com.example.bookappwithnavview.ui.book.BookFragment
import com.example.bookappwithnavview.ui.categorylist.CategoryFragment
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : DataBindingActivity<ActivityMainBinding>(), HasSupportFragmentInjector, Navigation {

    override val layoutId = R.layout.activity_main

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)


        val activity = this

        with(binding) {

            setSupportActionBar(toolBar)

            navView.setNavigationItemSelectedListener { menuItem ->
                menuItem.isChecked = true
                drawerLayout.closeDrawers()
                handleNavigationDrawerItemClick(menuItem.itemId)
                true
            }
        }

        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp)
        }

        with(viewModel) {
            showBooksEvent.observe(activity, Observer {
                navigateBook()
            })

            showCategoryEvent.observe(activity, Observer {
                navigateCategory()
            })
        }
    }

    private fun handleNavigationDrawerItemClick(itemId: Int) {
        when (itemId) {
            R.id.item_book -> {
                viewModel.openBookFragment()
            }

            R.id.item_category -> {
                viewModel.openCategoryFragment()
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        return when (item?.itemId) {
            android.R.id.home -> {
                binding.drawerLayout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    // navigation
    override fun navigateBook() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, BookFragment())
            .commit()
    }

    override fun navigateCategory() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, CategoryFragment())
            .commit()
    }

}