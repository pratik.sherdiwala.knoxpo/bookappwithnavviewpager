package com.example.bookappwithnavview.ui.categorylist.adapter.viewholder

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bookappwithnavview.R
import com.example.bookappwithnavview.model.Category

class CategoryVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mNameTV = itemView.findViewById<TextView>(R.id.nameTV)
    private val mIdTV = itemView.findViewById<TextView>(R.id.idTV)

    fun bindCategory(
        category: Category,
        categoryClick: (category: Category) -> Unit
    ) {
        mNameTV.text = category.name
        mIdTV.text = category.id

        itemView.setOnClickListener {
            categoryClick(category)
        }

    }
}