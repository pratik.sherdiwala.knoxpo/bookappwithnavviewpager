package com.example.bookappwithnavview.ui.book

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.example.bookappwithnavview.R
import com.example.bookappwithnavview.databinding.FragmentBookBinding
import com.example.bookappwithnavview.model.Category
import com.example.bookappwithnavview.ui.booklist.BookListFragment
import com.example.bookappwithnavview.ui.categorydetail.CategoryDetailActivity
import com.google.android.material.tabs.TabLayout
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.fragment_book.*
import javax.inject.Inject

private val TAG = BookFragment::class.java.simpleName

class BookFragment : Fragment(), HasSupportFragmentInjector, CategoryDetailActivity.Callback {


    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    private var itemList: List<Category>? = null

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentBookBinding

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(BookViewModel::class.java)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_category, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.addBTN -> {
                val position = viewPager.currentItem
                val value = viewPager.adapter?.getPageTitle(position)

                viewModel.addBook(
                    "Book Name",
                    value.toString()
                )
                Log.d(TAG, "Add book here")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fragment = this

        with(binding) {
            viewModel = fragment.viewModel
            tabLayout.setupWithViewPager(viewPager)
            tabLayout.tabMode = TabLayout.MODE_SCROLLABLE
        }

        with(viewModel) {
            commandShow.observe(fragment, Observer {
                Log.d(TAG, "Observed")
                it.getContentIfNotHandled()?.let {
                    itemList = it
                    setUpViePager(binding.viewPager)
                }
            })

            fetchCategories()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.binding = FragmentBookBinding.inflate(
            inflater,
            container,
            false
        )

        binding.lifecycleOwner = this
        return binding.root
    }

    private fun setUpViePager(viewPager: ViewPager) {

        val adapter = ViewPagerAdapter(childFragmentManager)

        for (category in itemList!!) {
            adapter.adddFragment(BookListFragment.newInstance(category.name), category.name)
        }
//
//     adapter.adddFragment(BookListFragment.newInstance(R.string.action), getString(R.string.action))
//        adapter.adddFragment(BookListFragment.newInstance(R.string.scifi), getString(R.string.scifi))
//        adapter.adddFragment(BookListFragment.newInstance(R.string.romance), getString(R.string.romance))
//        adapter.adddFragment(BookListFragment.newInstance(R.string.education), getString(R.string.education))
//        adapter.adddFragment(BookListFragment.newInstance(R.string.thriller), getString(R.string.thriller))

        viewPager.adapter = adapter
    }

    override fun sendcategory(title: String) {

    }
}