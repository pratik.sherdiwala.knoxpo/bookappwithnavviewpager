package com.example.bookappwithnavview.ui._common

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.bookappwithnavview.model.Book
import com.example.bookappwithnavview.model.Category
import com.example.bookappwithnavview.ui.booklist.adapter.BookAdapter
import com.example.bookappwithnavview.ui.categorylist.adapter.CategoryAdapter

object DataBindingAdapter {

    @JvmStatic
    @BindingAdapter("items")
    fun setBook(view: RecyclerView, items: List<Book>?) {
        items?.let {
            (view.adapter as? BookAdapter)?.updateBook(it)
        }
    }

    @JvmStatic
    @BindingAdapter("items")
    fun setCategory(view: RecyclerView, items: List<Category>?) {
        items?.let {
            (view.adapter as? CategoryAdapter)?.updateCategory(it)
        }
    }
}