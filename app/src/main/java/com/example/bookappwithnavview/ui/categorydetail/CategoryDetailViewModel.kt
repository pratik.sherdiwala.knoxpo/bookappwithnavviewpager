package com.example.bookappwithnavview.ui.categorydetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookappwithnavview.Event
import com.example.bookappwithnavview.data.repository.CategoryRepository
import com.example.bookappwithnavview.model.Book
import com.example.bookappwithnavview.model.Category
import javax.inject.Inject

class CategoryDetailViewModel @Inject constructor(private val categoryRepository: CategoryRepository) : ViewModel() {

    val id = MutableLiveData<String>()
    val name = MutableLiveData<String>()

    private val _categoryAddedEvent = MutableLiveData<Event<Unit>>()
    val categoryAddedEvent: LiveData<Event<Unit>>
        get() = _categoryAddedEvent


    fun addCategory() {
        val category = Category(
            name = name.value!!,
            id = id.value!!
        )
        categoryRepository.addCategory(category)
        _categoryAddedEvent.value = Event(Unit)
    }
}