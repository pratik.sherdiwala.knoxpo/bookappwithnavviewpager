package com.example.bookappwithnavview.ui.booklist.adapter.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bookappwithnavview.R
import com.example.bookappwithnavview.model.Book

class BookVH(view: View) : RecyclerView.ViewHolder(view) {

    private val mCoverIV = itemView.findViewById<ImageView>(R.id.coverIV)
    private val mName = itemView.findViewById<TextView>(R.id.nameTV)
    private val mGenre = itemView.findViewById<TextView>(R.id.genreIV)
    private val mId = itemView.findViewById<TextView>(R.id.idTV)
    private val mAuthor = itemView.findViewById<TextView>(R.id.authorTV)

    fun bindBook(book: Book) {
        mName.text = book.name
        mGenre.text = book.genre
        mId.text = book.id
        mAuthor.text = book.author
    }
}