package com.example.bookappwithnavview.ui.book

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookappwithnavview.Event
import com.example.bookappwithnavview.data.repository.CategoryRepository
import com.example.bookappwithnavview.model.Category
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class BookViewModel @Inject constructor(private val categoryRepository: CategoryRepository) : ViewModel() {


    val disposable = CompositeDisposable()

    val commandShow = MutableLiveData<Event<List<Category>>>()

    fun fetchCategories() {
        categoryRepository.getCategories()
            .subscribe(
                {
                    commandShow.value = Event(it)
                },
                {

                },
                {

                }
            )
            .also {
                disposable.add(it)
            }
    }

    fun addBook(bookName: String, categoryName: String) {
        categoryRepository.addBookToCategory(bookName, categoryName)
    }
}