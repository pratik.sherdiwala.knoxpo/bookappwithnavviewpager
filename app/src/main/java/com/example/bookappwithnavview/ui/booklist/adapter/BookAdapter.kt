package com.example.bookappwithnavview.ui.booklist.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bookappwithnavview.R
import com.example.bookappwithnavview.model.Book
import com.example.bookappwithnavview.ui.booklist.adapter.viewholder.BookVH

private val TAG = BookAdapter::class.java.simpleName

class BookAdapter : RecyclerView.Adapter<BookVH>() {

    private var items: List<Book>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookVH {
        return BookVH(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_list_book,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount() = items?.size ?: 0

    override fun onBindViewHolder(holder: BookVH, position: Int) {
        holder.bindBook(items!![position])
    }

    fun updateBook(newBook: List<Book>) {
        items = newBook
        Log.d(TAG, "Books $items")
        notifyDataSetChanged()
    }
}