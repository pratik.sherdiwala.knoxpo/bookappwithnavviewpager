package com.example.bookappwithnavview.ui.categorylist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookappwithnavview.Event
import com.example.bookappwithnavview.data.repository.CategoryRepository
import com.example.bookappwithnavview.model.Category
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CategoryViewModel @Inject constructor(private val categoryRepository: CategoryRepository) : ViewModel() {

    val itemList = MutableLiveData<List<Category>>()

    private val disposable = CompositeDisposable()


    private val _openTaskEvent = MutableLiveData<Event<String>>()

    val openTaskEvent: LiveData<Event<String>>
        get() = _openTaskEvent

    internal fun openTask(taskId: String) {
        _openTaskEvent.value = Event(taskId)
    }

    fun fetchCategories() {
        categoryRepository.getCategories()
            .subscribe(
                {
                    itemList.value = it
                },
                {

                },
                {

                }
            )
            .also {
                disposable.add(it)
            }
    }


}