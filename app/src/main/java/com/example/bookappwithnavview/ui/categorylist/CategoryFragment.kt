package com.example.bookappwithnavview.ui.categorylist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bookappwithnavview.R
import com.example.bookappwithnavview.TaskNavigator
import com.example.bookappwithnavview.databinding.FragmentCategoryBinding
import com.example.bookappwithnavview.ui.categorylist.adapter.CategoryAdapter
import com.example.bookappwithnavview.ui.categorydetail.CategoryDetailActivity
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

private val TAG = CategoryFragment::class.java.simpleName

class CategoryFragment : Fragment(), TaskNavigator {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentCategoryBinding

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        )
            .get(CategoryViewModel::class.java)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_category, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.addBTN -> {
                this.addnewTask()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fragment = this

        with(binding) {
            viewModel = fragment.viewModel

            categoryRV.layoutManager = LinearLayoutManager(context)
            categoryRV.adapter = CategoryAdapter(fragment.viewModel)
        }


        with(viewModel) {
            openTaskEvent.observe(fragment,
                Observer { event ->
                    event.getContentIfNotHandled()?.let {
                        fragment.addnewTask()
                    }
                })
            fetchCategories()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.binding = FragmentCategoryBinding.inflate(
            inflater,
            container,
            false
        )
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun addnewTask() {
        startActivity(
            CategoryDetailActivity.intentForAddCategory(context!!)
        )
    }
}