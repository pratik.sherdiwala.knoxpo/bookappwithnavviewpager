package com.example.bookappwithnavview.ui.main

interface Navigation {

    fun navigateBook()
    fun navigateCategory()

}