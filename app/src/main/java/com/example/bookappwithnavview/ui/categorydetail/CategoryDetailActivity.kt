package com.example.bookappwithnavview.ui.categorydetail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.DialogTitle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.bookappwithnavview.R
import com.example.bookappwithnavview.databinding.ActivityCategoryEditBinding
import com.example.bookappwithnavview.model.Category
import com.example.bookappwithnavview.ui._common.DataBindingActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_category_edit.*
import javax.inject.Inject

class CategoryDetailActivity : DataBindingActivity<ActivityCategoryEditBinding>() {

    interface Callback {

        fun sendcategory(title: String)

    }

    lateinit var mCallback: Callback

    companion object {

        private val TAG = CategoryDetailActivity::class.java.simpleName

        private const val ACTION_ADD = 0
        private const val ACTION_EDIT = 1

        private val EXTRA_ACTION = "$TAG>EXTRA_ACTION"

        internal fun intentForAddCategory(context: Context): Intent {
            return Intent(context, CategoryDetailActivity::class.java)
                .apply {
                    putExtra(EXTRA_ACTION, ACTION_ADD)
                }
        }
    }

    override val layoutId = R.layout.activity_category_edit

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        )
            .get(CategoryDetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        with(binding) {
            viewModel = activity.viewModel
        }

        with(viewModel) {
            categoryAddedEvent.observe(activity, Observer {
                it.getContentIfNotHandled()?.let {
                    finish()
                }
            })
        }

        when (intent.getIntExtra(EXTRA_ACTION, -1)) {
            ACTION_ADD -> {
                updateBTN.setOnClickListener {
                    addCategory()
                }
                updateBTN.text = "CREATE"
            }
        }
    }

    private fun addCategory() {
        with(viewModel) {
            addCategory()
        }
    }
}