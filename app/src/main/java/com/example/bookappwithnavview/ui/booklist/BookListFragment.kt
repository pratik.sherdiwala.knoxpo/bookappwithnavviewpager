package com.example.bookappwithnavview.ui.booklist

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bookappwithnavview.databinding.FragmentBookListBinding
import com.example.bookappwithnavview.ui.booklist.adapter.BookAdapter
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

private val TAG = BookListFragment::class.java.simpleName

class BookListFragment : Fragment() {

    companion object {

        private val ARGS_TITLE = "$TAG.ARGS_TITLE"

        fun newInstance(title: String) = BookListFragment().apply {
            arguments = Bundle().apply {
                putString(ARGS_TITLE, title)
            }
        }

        /*
        {
            /*
            val fragment = BookListFragment()
            val args = Bundle()
            args.putInt(ARGS_TITLE, title)
            fragment.arguments = args
            return fragment
            */


        }
        */
    }

    private val title: String
        get() {
            return arguments!!.getString(ARGS_TITLE)!!
        }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentBookListBinding

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        )
            .get(BookListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        binding.itemsRV.adapter?.notifyDataSetChanged()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.d(TAG, "onViewCreated")

        val fragment = this

        with(binding) {
            viewModel = fragment.viewModel

            itemsRV.layoutManager = LinearLayoutManager(context)
            itemsRV.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

            itemsRV.adapter = BookAdapter()
        }

        viewModel.getBooks(title)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.binding = FragmentBookListBinding.inflate(
            inflater,
            container,
            false
        )

        binding.lifecycleOwner = this
        return binding.root
    }
}