package com.example.bookappwithnavview.ui.booklist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookappwithnavview.data.repository.Bookrepository
import com.example.bookappwithnavview.model.Book
import javax.inject.Inject

class BookListViewModel @Inject constructor(private val bookrepository: Bookrepository) : ViewModel() {

    val itemList = MutableLiveData<List<Book>>()

    fun getBooks(type: String) {
        itemList.value = bookrepository.getBooks(type)
    }

}