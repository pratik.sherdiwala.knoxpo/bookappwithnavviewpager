package com.example.bookappwithnavview.data.repository

import android.util.Log
import com.example.bookappwithnavview.data.local.CategoryLab
import com.example.bookappwithnavview.model.Book
import com.example.bookappwithnavview.model.Category
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CategoryRepository @Inject constructor(private val categoryLab: CategoryLab) {

    private val categoryListSubject = BehaviorSubject.createDefault<List<Category>>(categoryLab.categoryList)

    fun getCategoryList(): List<Category> {
        return categoryLab.categoryList
    }

    fun addCategory(category: Category) {
        categoryLab.addCategory(category)
        categoryListSubject.onNext(categoryLab.categoryList)
    }

    fun getCategories(): Observable<List<Category>> {
        return categoryListSubject
    }

    fun addBookToCategory(bookName: String, categoryName: String) {

        categoryLab.categoryList.find {
            it.name == categoryName
        }
            ?.bookList
            ?.add(
                Book(
                    "123",
                    bookName,
                    "",
                    "",
                    ""
                )
            )

        Log.d("", "")

    }
}