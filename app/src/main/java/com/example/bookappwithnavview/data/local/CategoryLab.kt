package com.example.bookappwithnavview.data.local

import com.example.bookappwithnavview.model.Category
import javax.inject.Inject

class CategoryLab @Inject constructor(bookLab: BookLab) {

    val categoryList by lazy {
        mutableListOf(
            Category(
                "1",
                "Thiller",
                bookLab.thrillerList
            ),
            Category(
                "2",
                "Action",
                bookLab.actionList
            ),
            Category(
                "3",
                "Sci-Fi",
                bookLab.sciFiList
            ),
            Category(
                "4",
                "Education",
                bookLab.educationList
            ),
            Category(
                "5",
                "Romance",
                bookLab.romanceList
            )
        )
    }

    fun addCategory(category: Category) {
        //category.id = (categoryList.size + 1).toString()
        categoryList.add(category)
    }
}