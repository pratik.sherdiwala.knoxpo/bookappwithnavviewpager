package com.example.bookappwithnavview.data.repository

import android.content.Context
import com.example.bookappwithnavview.R
import com.example.bookappwithnavview.data.local.BookLab
import com.example.bookappwithnavview.data.local.CategoryLab
import com.example.bookappwithnavview.model.Book
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Bookrepository @Inject constructor(
    private val context: Context,
    private val bookLab: BookLab,
    private val categoryLab: CategoryLab
) {

    fun getBooks(categoryName: String): List<Book> {

        val foundCategory = categoryLab.categoryList.find {
            it.name == categoryName
        }

        return foundCategory?.bookList ?: listOf()
    }
}