package com.example.bookappwithnavview.di.module

import com.example.bookappwithnavview.ui.book.BookFragment
import com.example.bookappwithnavview.ui.booklist.BookListFragment
import com.example.bookappwithnavview.ui.categorylist.CategoryFragment
import com.example.bookappwithnavview.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector(
        modules = [BookFragmentModule::class,
            CategoryFragmentModule::class]
    )
    abstract fun contributeMainActivity(): MainActivity
}

@Module
abstract class BookFragmentModule {
    @ContributesAndroidInjector(
        modules = [BookListFragmentModule::class]
    )
    abstract fun contributeBookFragment(): BookFragment
}

@Module
abstract class BookListFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeBookListFragment(): BookListFragment

}

@Module
abstract class CategoryFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeCategoryFragment(): CategoryFragment
}
