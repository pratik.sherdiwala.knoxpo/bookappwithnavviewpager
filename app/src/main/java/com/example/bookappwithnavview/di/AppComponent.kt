package com.example.bookappwithnavview.di

import android.app.Application
import android.content.Context
import com.example.bookappwithnavview.App
import com.example.bookappwithnavview.di.module.CategoryEditModule
import com.example.bookappwithnavview.di.module.ContextModule
import com.example.bookappwithnavview.di.module.MainActivityModule
import com.example.bookappwithnavview.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        ContextModule::class,
        MainActivityModule::class,
        ViewModelModule::class,
        CategoryEditModule::class
    ]
)


interface AppComponent {

    @Component.Builder
    interface Builder{

        @BindsInstance
        fun context(context: Application): Builder

        fun create(): AppComponent
    }

    fun inject(app: App)

}