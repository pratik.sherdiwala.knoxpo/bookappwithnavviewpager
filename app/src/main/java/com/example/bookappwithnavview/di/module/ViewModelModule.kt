package com.example.bookappwithnavview.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bookappwithnavview.ui.booklist.BookListViewModel
import com.example.bookappwithnavview.ui.book.BookViewModel
import com.example.bookappwithnavview.ui.categorylist.CategoryViewModel
import com.example.bookappwithnavview.ui.categorydetail.CategoryDetailViewModel
import com.example.bookappwithnavview.ui.main.MainViewModel
import com.example.bookappwithnavview.viewmodel.AppViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    abstract fun provideViewModelFactory(appViewModelFactory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(BookListViewModel::class)
    abstract fun bindBookListFragmentModel(bookListViewModel: BookListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BookViewModel::class)
    abstract fun bindBookViewModel(bookViewModel: BookViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CategoryViewModel::class)
    abstract fun bindCategoryModule(categoryViewModel: CategoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CategoryDetailViewModel::class)
    abstract fun bindCategoryEditViewModel(categoryDetailViewModel: CategoryDetailViewModel): ViewModel

}