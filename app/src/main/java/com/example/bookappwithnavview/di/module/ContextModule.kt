package com.example.bookappwithnavview.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ContextModule {

    @Provides
    fun provideContext(app: Application): Context{
        return app
    }

}