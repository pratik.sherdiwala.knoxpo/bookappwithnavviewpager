package com.example.bookappwithnavview.di.module

import com.example.bookappwithnavview.ui.categorydetail.CategoryDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class CategoryEditModule {

    @ContributesAndroidInjector
    abstract fun contributeCategoryEditModule(): CategoryDetailActivity


}