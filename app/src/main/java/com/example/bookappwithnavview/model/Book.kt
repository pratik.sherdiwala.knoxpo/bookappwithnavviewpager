package com.example.bookappwithnavview.model

data class Book(
    val id: String,
    val name: String,
    val author: String,
    val genre: String,
    val cover: String
)