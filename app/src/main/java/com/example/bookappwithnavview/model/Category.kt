package com.example.bookappwithnavview.model

data class Category(
    var id: String,
    var name: String,
    var bookList: MutableList<Book> = mutableListOf()
)